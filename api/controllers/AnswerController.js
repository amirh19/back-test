/**
 * AnswerController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  create: async (req, res) => {
    if (req.headers["content-type"] !== "application/json") {
      return res.status(406).send();
    }
    try {
      if (
        !req.param("type") ||
        typeof req.param("type") !== "string" ||
        !req.param("questionId") ||
        typeof req.param("questionId") !== "string"
      ) {
        return res.badRequest("Invalid param type");
      }
      let answer;
      switch (req.param("type")) {
        case "boolean":
          if (typeof req.param("boolean") !== "boolean")
            return res.badRequest("Unexpected type,expected boolean");
          answer.boolean = req.param("boolean");
          break;
        case "onlyAnswer":
          if (
            typeof req.param("onlyAnswer") !== "number" ||
            parseInt(req.param("onlyAnswer") === NaN)
          )
            return res.badRequest("Unexpected type, expected integer");
          answer.onlyAnswer = parseInt(req.param("onlyAnswer"));
          break;
        case "multiple":
        case "multipleAll":
          if (!Array.isArray(req.param(req.param("type")))) {
            return res.badRequest("Unexpected type, expected array");
          } else {
            req.param(req.param("type")).forEach((element) => {
              if (typeof element !== "number" && parseInt(element) !== NaN)
                return res.badRequest(
                  "Unexpected element type, expected integer"
                );
            });
            answer[req.param("type")] = req.param(req.param("type"));
          }
          break;
        default:
          return badRequest("Invalid type");
      }
      answer.question = req.param("questionId");
      await Answer.create(answer);
      return res.ok();
    } catch (error) {
      if (error.name && error.name === "AdapterError") {
        return res.badRequest("User already exists");
      }
      console.error(error);
      return res.serverError();
    }
  },
  read: async (req, res) => {
    try {
      if (!req.param("answerId") || typeof req.param("answerId") !== "string") {
        return res.badRequest();
      }
      const answer = await Answer.findOne({
        id: req.param("answerId"),
      }).populate("question");
      if (!answer) {
        return res.notFound();
      }
      return res.json(answer);
    } catch (error) {
      return res.serverError();
    }
  },
  update: async (req, res) => {
    try {
      const params = req.allParams();
      if (
        !req.param("type") ||
        typeof req.param("type") !== "string" ||
        !req.param("answerId") ||
        typeof req.param("answerId") !== "string"
      ) {
        return res.badRequest("Invalid param type");
      }
      let answer = await Answer.findOne({ id: req.param("answerId") });
      if (!answer) {
        return res.notFound();
      }
      if (params.type && typeof params.type === "string") {
        answer.type = params.type;
      }
      switch (req.param("type")) {
        case "boolean":
          if (typeof req.param("boolean") !== "boolean")
            return res.badRequest("Unexpected type,expected boolean");
          answer.boolean = req.param("boolean");
          break;
        case "onlyAnswer":
          if (
            typeof req.param("onlyAnswer") !== "number" ||
            parseInt(req.param("onlyAnswer") === NaN)
          )
            return res.badRequest("Unexpected type, expected integer");
          answer.onlyAnswer = parseInt(req.param("onlyAnswer"));
          break;
        case "multiple":
        case "multipleAll":
          if (!Array.isArray(req.param(req.param("type")))) {
            return res.badRequest("Unexpected type, expected array");
          } else {
            req.param(req.param("type")).forEach((element) => {
              if (typeof element !== "number" && parseInt(element) !== NaN)
                return res.badRequest(
                  "Unexpected element type, expected integer"
                );
            });
            answer[req.param("type")] = req.param(req.param("type"));
          }
          break;
        default:
          return badRequest("Invalid type");
      }
      await Answer.update({ id: params.answerId }).set(answer);
      return res.ok();
    } catch (error) {
      console.error(error);
      return res.serverError();
    }
  },
  delete: async (req, res) => {
    try {
      const params = req.allParams();
      if (!params.answerId || typeof params.answerId !== "string") {
        return res.badRequest();
      }
      await Answer.destroyOne({ id: params.answerId });
      res.ok();
    } catch (error) {
      console.error(error);
      return res.serverError();
    }
  },
};
