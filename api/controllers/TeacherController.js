/**
 * TeacherController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  create: async (req, res) => {
    if (req.headers["content-type"] !== "application/json") {
      return res.status(406).send();
    }
    try {
      if (
        !req.param("name") ||
        typeof req.param("name") !== "string" ||
        !req.param("user") ||
        typeof req.param("user") !== "string" ||
        !req.param("password") ||
        typeof req.param("password") !== "string"
      ) {
        return res.badRequest("Invalid param type");
      }

      await Teacher.create({
        name: req.param("name"),
        user: req.param("user"),
        password: req.param("password"),
      });
      return res.ok();
    } catch (error) {
      if (error.name && error.name === "AdapterError") {
        return res.badRequest("User already exists");
      }
      console.error(error);
      return res.serverError();
    }
  },
  read: async (req, res) => {
    try {
      if (
        !req.param("teacherId") ||
        typeof req.param("teacherId") !== "string"
      ) {
        return res.badRequest();
      }
      const teacher = await Teacher.findOne({
        id: req.param("teacherId"),
      }).populate("createdCourses");
      if (!teacher) {
        return res.notFound();
      }
      return res.json(teacher);
    } catch (error) {
      return res.serverError();
    }
  },
  update: async (req, res) => {
    try {
      const params = req.allParams();
      if (!params.teacherId || typeof params.teacherId !== "string") {
        return res.badRequest();
      }
      let teacher = await Teacher.findOne({ id: params.teacherId });
      if (!teacher) {
        return res.notFound();
      }
      if (params.name && typeof params.name === "string") {
        teacher.name = params.name;
      }
      if (params.user && typeof params.user === "string") {
        teacher.user = params.user;
      }
      if (params.password && typeof params.password === "string") {
        teacher.password = params.password;
      }
      await Teacher.update({ id: params.teacherId }).set(teacher);
      return res.ok();
    } catch (error) {
      console.error(error);
      return res.serverError();
    }
  },
  delete: async (req, res) => {
    try {
      const params = req.allParams();
      if (!params.teacherId || typeof params.teacherId !== "string") {
        return res.badRequest();
      }
      await Teacher.destroyOne({ id: params.teacherId });
      res.ok();
    } catch (error) {
      console.error(error);
      return res.serverError();
    }
  },
};
