/**
 * QuestionController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  create: async (req, res) => {
    if (req.headers["content-type"] !== "application/json") {
      return res.status(406).send();
    }
    try {
      const params = req.allParams();
      if (
        !params.score ||
        typeof params.score !== "number" ||
        !params.lessonId ||
        typeof params.lessonId !== "string" ||
        !params.description ||
        typeof params.description !== "string"
      ) {
        return res.badRequest("Invalid param type");
      }
      const lesson = await Lesson.findOne({ id: req.param("lessonId") });
      if (!lesson) {
        //if no lesson found, then throw a bad request
        return res.notFound();
      }
      const question = await Question.create({
        score: req.param("score"),
        description: req.param("description"),
        lesson: req.param("lessonId"),
      }).fetch();
      return res.ok();
    } catch (error) {
      console.error(error);
      res.serverError();
    }
  },
  read: async (req, res) => {
    try {
      if (
        !req.param("questionId") ||
        typeof req.param("questionId") !== "string"
      ) {
        return res.badRequest();
      }
      const question = await Question.findOne({
        id: req.param("questionId"),
      }).populate("answers");
      if (!question) {
        return res.notFound();
      }
      return res.json(question);
    } catch (error) {
      console.error(error);
      return res.serverError();
    }
  },
  update: async (req, res) => {
    try {
      const params = req.allParams();
      if (!params.questionId || typeof params.questionId !== "string") {
        return res.badRequest();
      }
      let question = await Question.findOne({ id: params.questionId });
      if (!question) {
        return res.notFound();
      }
      if (params.score && typeof params.score === "number") {
        question.score = params.score;
      }
      if (params.description && typeof params.description === "string") {
        question.description = params.description;
      }
      if (params.lessonId && typeof params.lessonId === "string") {
        question.lessonId = params.lessonId;
      }
      await Question.update({ id: params.questionId }).set(question);
      return res.ok();
    } catch (error) {
      console.error(error);
      return res.serverError();
    }
  },
  delete: async (req, res) => {
    try {
      const params = req.allParams();
      if (!params.questionId || typeof params.questionId !== "string") {
        return res.badRequest();
      }
      await Question.destroyOne({ id: params.questionId });
      res.ok();
    } catch (error) {
      console.error(error);
      return res.serverError();
    }
  },
};
