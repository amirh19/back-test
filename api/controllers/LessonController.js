/**
 * LessonController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  create: async (req, res) => {
    if (req.headers["content-type"] !== "application/json") {
      return res.status(406).send();
    }
    try {
      const params = req.allParams();
      if (
        !params.name ||
        typeof params.name !== "string" ||
        !params.approvalScore ||
        typeof params.approvalScore !== "number" ||
        !params.courseId ||
        typeof params.courseId !== "string" ||
        typeof params.previousLesson !== "string" ||
        !params.description ||
        typeof params.description !== "string"
      ) {
        return res.badRequest("Invalid param type");
      }
      const course = await Course.findOne({ id: req.param("courseId") });
      if (!course) {
        //if no course found, then throw a bad request
        return res.notFound();
      }
      await Lesson.create({
        name: req.param("name"),
        description: req.param("description"),
        approvalScore: req.param("approvalScore"),
        belongsTo: req.param("courseId"),
        previousLesson: req.param("previousLesson") || null,
      }).fetch();
      return res.ok();
    } catch (error) {
      console.error(error);
      res.serverError();
    }
  },
  read: async (req, res) => {
    try {
      if (!req.param("lessonId") || typeof req.param("lessonId") !== "string") {
        return res.badRequest();
      }
      const lesson = await Lesson.findOne({
        id: req.param("lessonId"),
      })
        .populate("previousLesson")
        .populate("questions")
        .populate("belongsTo");
      if (!lesson) {
        return res.notFound();
      }
      return res.json(lesson);
    } catch (error) {
      return res.serverError();
    }
  },
  update: async (req, res) => {
    try {
      const params = req.allParams();
      if (!params.lessonId || typeof params.lessonId !== "string") {
        return res.badRequest();
      }
      let lesson = await Lesson.findOne({ id: params.lessonId });
      if (!lesson) {
        return res.notFound();
      }
      if (params.name && typeof params.name === "string") {
        lesson.name = params.name;
      }
      if (params.description && typeof params.description === "string") {
        lesson.description = params.description;
      }
      if (param.approvalScore && typeof params.approvalScore === "number") {
        lesson.approvalScore = params.approvalScore;
      }
      if (params.courseId && typeof params.courseId === "string") {
        lesson.belongsTo = params.courseId;
      }
      if (params.previousLesson && typeof params.previousLesson === "string") {
        lesson.previousLesson = params.previousLesson;
      }
      await Lesson.update({ id: params.lessonId }).set(lesson);
      return res.ok();
    } catch (error) {
      console.error(error);
      return res.serverError();
    }
  },
  delete: async (req, res) => {
    try {
      const params = req.allParams();
      if (!params.lessonId || typeof params.lessonId !== "string") {
        return res.badRequest();
      }
      await Lesson.destroyOne({ id: params.lessonId });
      res.ok();
    } catch (error) {
      console.error(error);
      return res.serverError();
    }
  },
};
