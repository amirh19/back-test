/**
 * StudentController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  create: async (req, res) => {
    if (req.headers["content-type"] !== "application/json") {
      return res.status(406).send();
    }
    try {
      if (
        !req.param("name") ||
        typeof req.param("name") !== "string" ||
        !req.param("user") ||
        typeof req.param("user") !== "string" ||
        !req.param("password") ||
        typeof req.param("password") !== "string"
      ) {
        return res.badRequest("Invalid param type");
      }

      await Student.create({
        name: req.param("name"),
        user: req.param("user"),
        password: req.param("password"),
      });
      return res.ok();
    } catch (error) {
      if (error.name && error.name === "AdapterError") {
        return res.badRequest("User already exists");
      }
      console.error(error);
      return res.serverError();
    }
  },
  read: async (req, res) => {
    try {
      if (
        !req.param("studentId") ||
        typeof req.param("studentId") !== "string"
      ) {
        return res.badRequest();
      }
      const student = await Student.findOne({
        id: req.param("studentId"),
      });
      if (!student) {
        return res.notFound();
      }
      return res.json(student);
    } catch (error) {
      return res.serverError();
    }
  },
  update: async (req, res) => {
    try {
      const params = req.allParams();
      if (!params.studentId || typeof params.studentId !== "string") {
        return res.badRequest();
      }
      let student = await Student.findOne({ id: params.studentId });
      if (!student) {
        return res.notFound();
      }
      if (params.name && typeof params.name === "string") {
        student.name = params.name;
      }
      if (params.user && typeof params.user === "string") {
        student.user = params.user;
      }
      if (params.password && typeof params.password === "string") {
        student.password = params.password;
      }
      await Student.update({ id: params.studentId }).set(student);
      return res.ok();
    } catch (error) {
      console.error(error);
      return res.serverError();
    }
  },
  delete: async (req, res) => {
    try {
      const params = req.allParams();
      if (!params.studentId || typeof params.studentId !== "string") {
        return res.badRequest();
      }
      await Student.destroyOne({ id: params.studentId });
      res.ok();
    } catch (error) {
      console.error(error);
      return res.serverError();
    }
  },
  takeCourse: async (req, res) => {
    try {
      const params = req.allParams();
      if (
        !params.courseId ||
        typeof params.courseId !== "string" ||
        !params.studentId ||
        typeof params.studentId !== "string"
      ) {
        return res.badRequest("Invalid param type");
      }
      const desiredCourse = await Course.findOne({
        id: req.param("courseId"),
      }).populate("previousCourse");
      // if desired course has a previous one
      if (desiredCourse.previousCourse) {
        const previousCourseTaken = await InProgress.findOne({
          student: req.param("studentId"),
          course: desiredCourse.previousCourse.id,
        });
        //if previous one has not been taken or approved
        if (!previousCourseTaken || !previousCourseTaken.approved) {
          return res.forbidden("El estudiante no ha aprobado el curso previo");
        } else {
          //otherwise
          await InProgress.create({
            student: req.param("studentId"),
            course: req.param("courseId"),
          });
          return res.ok();
        }
      } else {
        // if course has not a previous one.
        await InProgress.create({
          student: req.param("studentId"),
          course: req.param("courseId"),
        });
        return res.ok();
      }
    } catch (error) {
      console.error(error);
      return res.serverError();
    }
  },
  /**
   * @description Used to approve lessons and courses.
   * @param {Request} req Request
   * @param {Response} res Response
   * @property {Object} req.params Request parameters
   * @property {string } req.params.studentId Student's id
   * @property {string } req.params.lessonId Lesson's id
   * @property {string } req.params.courseId Course's id
   * @property {Object[]} req.params.answers The student answers
   * @property {("boolean"||"onlyAnswer"||"multiple"||"multipleAll")} req.params.answers[].type Answer type
   * @property {boolean|| integer || integer[]} req.params.answers[].value Answer
   */
  takeLesson: async (req, res) => {
    try {
      if (
        !req.param("lessonId") ||
        typeof req.param("lessonId") !== "string" ||
        !req.param("studentId") ||
        typeof req.param("studentId") !== "string" ||
        !req.param("answers") ||
        !typeof req.param("answers") !== "object" ||
        !req.param("courseId") ||
        typeof req.param("courseId") !== "string"
      ) {
        return res.badRequest();
      }
      let lesson = await Lesson.findOne({ id: req.param("lessonId") });
      if (!lesson) {
        return res.notFound();
      }
      const questions = await Promise.all(
        lesson.questions.map(
          async (question) =>
            await Question.findOne({ id: question.id }).populate("answers")
        )
      );

      lesson.questions = questions;

      //getting score points;
      const points = req.param("answers").reduce((acc, answer) => {
        //find the question
        const question = lesson.questions.find(
          (item) => item.id === answer.questionId
        );
        //compare values
        switch (answer.type) {
          case "boolean":
          case "onlyAnswer":
            if (question.answers[0][answer.type] === answer.value) {
              //if value matches, then add question score to final points
              return acc + question.score;
            } else {
              //else return curren final points
              return acc;
            }
          case "multiple":
            if (
              question.answer[0][answer.type].find(
                (item) => item === answer.value
              )
            ) {
              return acc + question.score;
            } else {
              return acc;
            }
          case "multipleAll":
            const found = question.answers[0][answer.type].map((item) =>
              //if the answer if
              answer.value.indexOf(item) !== -1 ? 1 : 0
            );
            if (
              question.answers[0][answer.type].length ===
              found.reduce((acc, val) => acc + val)
            ) {
              return acc + question.score;
            } else {
              return acc;
            }
          default:
            throw new Error("Type not found");
        }
        // end of reduce
      });
      if (points >= lesson.approvalScore) {
        //set the approved lesson id to the InProgress.
        approved = false;
        if (!(await Lesson.findOne({ previousLesson: lesson.id }))) {
          //if it is the last lesson, then approve course
          approved = true;
        }
        await InProgress.update({
          approved: approved,
          student: req.param("studentId"),
          course: req.param("courseId"),
        }).set({ lesson: lesson.id });
        return res.ok("success");
      } else {
        return res.ok("fail");
      }
    } catch (error) {
      console.error(error);
      return res.serverError();
    }
  },
  /**
   * @description Search courses and verify if they are available to the student.
   * @param {Request} req Request
   * @param {Response} res Response
   * @property {Object} req.params Params
   * @property {string} req.params.studentId Student's id
   */
  getCourses: async (req, res) => {
    try {
      const params = req.allParams();
      if (!params.studentId || typeof params.studentId !== "string") {
        return res.badRequest("Invalid param type");
      }
      let courses = await Course.find();
      if (!courses.length) {
        return res.notFound("No hay cursos disponibles");
      }
      //find all finished courses.
      let finished = await InProgress.find({
        student: params.studentId,
        approved: true,
      });
      if (!finished.length) {
        // if no finished courses, set available first courses

        return res.json({
          courses: courses.map((course) => ({
            ...course,
            available: !course.previousCourse,
          })),
        });
      } else {
        // if finished courses, then set next courses to available;

        //get finished courses id
        const ids = finished.map((course) => course.id);

        //search finished courses or next courses
        let available = await Course.find({
          or: [{ id: ids }, { previousCourse: ids }],
        });
        //search complement
        let notAvailable = await Course.find({
          id: {
            "!=": ids,
          },
          previousCourse: {
            "!=": ids,
          },
        });
        available = available.map((course) => ({
          ...course,
          available: true,
        }));
        //find next courses;
        notAvailable = notAvailable.map((course) => ({
          ...course,
          available: false,
        }));
        return res.json({
          courses: [...available, ...notAvailable],
        });
      }
    } catch (error) {
      console.error(error);
      res.serverError();
    }
  },
  getLessons: async (req, res) => {
    try {
      const params = req.allParams();
      if (
        !params.courseId ||
        typeof params.courseId !== "string" ||
        !params.studentId ||
        typeof params.studentId !== "string"
      ) {
        return res.badRequest("Invalid param type");
      }
      const inProgressCourse = await InProgress.findOne({
        student: params.studentId,
        course: params.courseId,
      });
      if (!inProgressCourse) {
        return res.notFound("No se ha iniciado este curso");
      }
      let lessons = [];
      if (inProgressCourse.approved) {
        lessons = await Lesson.find();
        return res.json({
          lessons: lessons.map((lesson) => {
            lesson.available = true;
            return lesson;
          }),
        });
      } else {
        let currentLessonId = inProgressCourse.lesson;
        let available = await Lesson.find({
          or: [
            {
              id: currentLessonId,
            },
            {
              previousLesson: currentLessonId,
            },
          ],
        });
        let notAvailable = await Lesson.find({
          id: { "!=": currentLessonId },
          previousLesson: { "!=": currentLessonId },
        });
        available = available.map((lesson) => ({
          ...lesson,
          available: true,
        }));
        notAvailable = notAvailable.map((lesson) => ({
          ...lesson,
          available: false,
        }));
        return res.json({ lesson: [...available, ...notAvailable] });
      }
    } catch (error) {
      console.error(error);
      return res.serverError();
    }
  },
};
