/**
 * CourseController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  create: async (req, res) => {
    if (req.headers["content-type"] !== "application/json") {
      return res.status(406).send();
    }
    try {
      const params = req.allParams();
      if (
        !params.name ||
        typeof params.name !== "string" ||
        !params.teacherId ||
        typeof params.teacherId !== "string" ||
        !params.previousCourse ||
        typeof params.previousCourse !== "string"
      ) {
        return res.badRequest("Invalid param type");
      }
      await Course.create({
        name: req.param("name"),
        teacher: req.param("teacherId"),
        previousCourse: req.param("previousCourseId"),
      });
      return res.ok();
    } catch (error) {
      console.error(error);
      res.serverError();
    }
  },
  read: async (req, res) => {
    try {
      if (!req.param("courseId") || typeof req.param("courseId") !== "string") {
        return res.badRequest();
      }
      const course = await Course.findOne({ id: req.param("courseId") })
        .populate("previousCourse")
        .populate("teacher")
        .populate("hasLessons");
      if (!course) {
        return res.notFound();
      }
      return res.json(course);
    } catch (error) {
      return res.serverError();
    }
  },
  update: async (req, res) => {
    try {
      const params = req.allParams();
      if (!params.courseId || typeof params.courseId !== "string") {
        return res.badRequest();
      }
      let course = await Course.findOne({ id: params.courseId });
      if (!course) {
        return res.notFound();
      }
      if (params.name && typeof params.name === "string") {
        course.name = params.name;
      }
      if (params.teacherId && typeof params.teacherId === "string") {
        course.teacher = params.teacherId;
      }
      if (params.previousCourse && typeof params.previousCourse === "string") {
        course.previousCourse = params.previousCourse;
      }
      await Course.update({ id: params.courseId }).set(course);
      return res.ok();
    } catch (error) {
      console.error(error);
      return res.serverError();
    }
  },
  delete: async (req, res) => {
    try {
      const params = req.allParams();
      if (!params.courseId || typeof params.courseId !== "string") {
        return res.badRequest();
      }
      await Course.destroyOne({ id: params.courseId });
      res.ok();
    } catch (error) {
      console.error(error);
      return res.serverError();
    }
  },
};
