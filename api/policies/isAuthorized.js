module.exports = async (req, res, proceed) => {
  //simple policy, look for a teacher with provided id.
  if (
    req.param("teacherId") &&
    typeof req.param("teacherId") === "string" &&
    (await Teacher.findOne({ id: req.param("teacherId") }))
  ) {
    return proceed();
  }

  //--•
  // Otherwise, this request did not come from a logged-in user.
  return res.forbidden();
};
