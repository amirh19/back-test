/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {
  /***************************************************************************
   *                                                                          *
   * Make the view located at `views/homepage.ejs` your home page.            *
   *                                                                          *
   * (Alternatively, remove this and add an `index.html` file in your         *
   * `assets` directory)                                                      *
   *                                                                          *
   ***************************************************************************/

  "/": { view: "pages/homepage" },

  /***************************************************************************
   *                                                                          *
   * More custom routes here...                                               *
   * (See https://sailsjs.com/config/routes for examples.)                    *
   *                                                                          *
   * If a request to a URL doesn't match any of the routes in this file, it   *
   * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
   * not match any of those, it is matched against static assets.             *
   *                                                                          *
   ***************************************************************************/

  //Teacher
  "POST /teacher/create": "TeacherController.create",
  "GET /teacher/read/:teacherId": "TeacherController.read",
  "PATCH /teacher/update": "TeacherController.update",
  "DELETE /teacher/delete": "TeacherController.delete",

  //Course
  "POST /course/create": "CourseController.create",
  "GET /course/read/:courseId": "CourseController.read",
  "PATCH /course/update": "CourseController.update",
  "DELETE /course/delete": "CourseController.delete",

  //Lesson
  "POST /lesson/create": "LessonController.create",
  "GET /lesson/read/:lessonId": "LessonController.read",
  "PATCH /lesson/update": "LessonController.update",
  "DELETE /lesson/delete": "LessonController.delete",

  //Question
  "POST /question/create": "QuestionController.create",
  "GET /question/read/:questionId": "QuestionController.read",
  "PATCH /question/update": "QuestionController.update",
  "DELETE /question/delete": "QuestionController.delete",

  //Answers
  "POST /answer/create": "AnswerController.create",
  "GET /answer/read/:answerId": "AnswerController.read",
  "PATCH /answer/update": "AnswerController.update",
  "DELETE /answer/delete": "AnswerController.delete",

  //Student
  "POST /student/create": "StudentController.create",
  "GET /student/read/:studentId": "StudentController.read",
  "PATCH /student/update": "StudentController.update",
  "DELETE /student/delete": "StudentController.delete",
  "GET /student/getCourses/:studentId": "StudentController.getCourses",
  "GET /student/getLessons/:studentId/:courseId":
    "StudentController.getLessons",
  "POST /student/takeLesson": "StudentController.takeLesson",
  "POST /student/takeCourse": "StudentController.takeCourse",
};
